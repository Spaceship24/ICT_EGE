# 7470
'''
На вход алгоритма подаётся натуральное число N. Алгоритм строит по нему новое число R следующим образом.
1. Строится семеричная запись числа N.
2. К этой записи дописывается разряд по следующему правилу:

а) если семеричная запись оканчивается чётной цифрой, то к семеричной записи слева дописывается 6;
б) если семеричная запись оканчивается нечётной цифрой, то к семеричной записи слева дописывается 5;
Полученная таким образом запись является семеричной записью числа R.

Например, исходное число 10(10) = 13(7). 13 оканчивается нечётной цифрой, поэтому слева дописывается 5.
Итоговое число - 513(7) = 255(10).

Для скольких N из промежутка [343; 2401] значение R больше 14500?
'''

# Видео-объяснение: https://www.youtube.com/watch?v=xPjomh6RHOY
'''
def digit_to_7(n):
    s = ''
    while n > 0:
        s = str(n % 7) + s # Присоединяю слева к s
        n //= 7
    return s

k = 0
for N in range(1, 15_000):
    # Семеричная запись числа N
    N_7 = digit_to_7(N)
    if int(N_7[-1]) % 2 == 0:
        N_7 = '6' + N_7
    else:
        N_7 = '5'  + N_7
    R = int(N_7, 7)

    if R > 14_500:
        k += 1
print(k) # 13775
'''
