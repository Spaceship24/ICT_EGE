# 9774
'''
На вход алгоритма подаётся натуральное число N. Алгоритм строит по нему новое число R следующим образом.

1. Строится троичная запись числа N.
2. Далее эта запись обрабатывается по следующему правилу:
а) если число N делится на 3, то к этой записи дописываются две последние троичные цифры;

б) если число N на 3 не делится, то остаток от деления умножается на 5,
переводится в троичную запись и дописывается в конец числа.

ПРОВЕРИТЬ ПРАВИЛЬНОСТЬ РАБОТЫ АЛГОРИТМА

Полученная таким образом запись является троичной записью искомого числа R.

3. Результат переводится в десятичную систему и выводится на экран.

Например, для исходного числа 11 = 102(3) результатом является число 102101(3) = 307,
а для исходного числа 12 = 110(3) это число 11010(3) = 111.

Укажите минимальное число R, большее 133, которое может быть получено с помощью описанного алгоритма.
В ответе запишите это число в десятичной системе счисления.
'''

"""
def dec_to_3(n):
    res = ''
    while n > 0:
        res = str(n % 3) + res # Сохраняем остатки в прямом порядке, чтобы не переворачивать строку
        # Работает до 9 С.С.
        n = n // 3
    return(res)

# а) если число N делится на 3, то к этой записи дописываются две последние троичные цифры;
# б) если число N на 3 не делится, то остаток от деления умножается на 5
# переводится в троичную запись и дописывается в конец числа.
lst = []
for N in range(1, 100):
    N_3 = dec_to_3(N)

    if N % 3 == 0:
        R = N_3 + N_3[-2:]
    else:
        R = N_3 + dec_to_3( (N % 3) * 5)
    
    # ПРОВЕРИТЬ ПРАВИЛЬНОСТЬ РАБОТЫ АЛГОРИТМА
    # print(N, R)
    # if N == 12:
        # break

    # Укажите минимальное число R, большее 133, которое может быть получено с помощью описанного алгоритма.
    # В ответе запишите это число в десятичной системе счисления.
    if int(R, 3) > 133:
        lst.append(int(R, 3))

print(min(lst))
"""
